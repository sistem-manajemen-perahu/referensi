# Referensi 
**Javascript**
- https://youtube.com/playlist?list=PLFIM0718LjIWXagluzROrA-iBY9eeUt4w - WPU JS Dasar
- https://www.youtube.com/playlist?list=PLFIM0718LjIWB3YRoQbQh82ZewAGtE2-3 - WPU JS DOM
- https://www.youtube.com/playlist?list=PLFIM0718LjIUGpY8wmE41W7rTJo_3Y46- - WPU JS Lanjutan
- https://www.youtube.com/watch?v=SDROba_M42g&t=8336s - PZN JS Dasar
- https://www.youtube.com/watch?v=aviAyIK5oSU&t=191s - PZN JS OOP
- https://www.youtube.com/watch?v=LLT6EAtX-x8&list=PL-CtdCApEFH-I4CD6km3BcXqrhWAkY4et - PZN JS Async

**Vue**
- https://www.youtube.com/playlist?list=PLnQvfeVegcJbV2NGXpay7ulU6ybASzAlG - Vue Dasar (Nusendra)
- https://www.youtube.com/playlist?list=PLnQvfeVegcJbn7ZDOUNbRN25obY4htSlB - Vue Composition API (Nusendra)
- https://www.youtube.com/watch?v=4B6xzOwwy78&list=PLnQvfeVegcJbbmFmN5ZMnkylQXp6mb7gP - Vue Advance (Nusendra)

**AIS**
- https://faq.spire.com/spire-maritime-ais-data-sample-standard-format
- https://www.navcen.uscg.gov/?pageName=aisdataformats

**Leaflet**
- https://dosenit.com/tutorial/membuat-marker-penanda-pada-peta-leaflet
- https://github.com/perliedman/leaflet-realtime
- https://www.section.io/engineering-education/how-to-build-a-real-time-location-tracker-using-leaflet-js/
